from fastapi import APIRouter

import api.handlers.v1.pets as pets

v1_router = APIRouter(prefix="/v0.0.1")
v1_router.include_router(pets.router)
