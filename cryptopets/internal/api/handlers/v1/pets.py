import logging

from fastapi import APIRouter, Request
from fastapi.responses import ORJSONResponse

from db.models.models import Pet


router = APIRouter(prefix="/pets")


@router.get("/")
async def get_pets(request: Request):
    data = await Pet.get_all(request.app.mongo_client)
    return data


@router.post("/")
async def create_pet(request: Request, body: dict):
    try:
        await Pet(**body).create(request.app.mongo_client)
        return ORJSONResponse({"message": "pet created"})
    except Exception as err:
        logging.error(err)
        return ORJSONResponse({"message": "pet not created"}, status_code=500)
