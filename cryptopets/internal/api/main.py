from fastapi import FastAPI, APIRouter

from motor.motor_asyncio import AsyncIOMotorClient

from settings.api import API_CONFIG, SWAGGER_CONFIG
from settings.db import MONGO_CONFIG
from db.models.models import create_collections

from .handlers.v1 import v1_router


def create_api():
    api = FastAPI(
        **SWAGGER_CONFIG.dict(),
        debug=API_CONFIG.DEBUG,
    )

    main_router = APIRouter(prefix="/api")
    main_router.include_router(v1_router)
    api.include_router(main_router)

    @api.on_event("startup")
    async def on_startup():
        api.mongo_client = AsyncIOMotorClient(MONGO_CONFIG.build_uri()) # type:ignore
        await create_collections(api.mongo_client) # type:ignore

    return api


api = create_api()
