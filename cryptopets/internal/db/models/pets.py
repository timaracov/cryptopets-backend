import enum
import uuid

from pydantic import BaseModel

from settings.db import MONGO_CONFIG


class PetsChoice(enum.Enum):
    UP = 1
    DOWN = 0


class Pet(BaseModel):
    name: str
    image: str
    total_balance: float
    today_choice: PetsChoice

    _collection_name: str = "Pets"

    @staticmethod
    async def get_all(client):
        pet_collection = client[MONGO_CONFIG.MONGO_DB].get_collection("Pets")
        data = await pet_collection.find().to_list(1000)
        return data

    async def create(self, client):
        pet_collection = client[MONGO_CONFIG.MONGO_DB].get_collection("Pets")
        await pet_collection.insert_one({
            "_id": str(uuid.uuid4()),
            "name": self.name,
            "image": self.image,
            "total_balance": self.total_balance,
            "today_choice": self.today_choice.value,
        })
