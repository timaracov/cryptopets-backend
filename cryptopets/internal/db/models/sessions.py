import datetime as dt

from pydantic import BaseModel


class DailySession(BaseModel):
    date: dt.datetime
    start_price: float
    final_price: float
    pets_profits: list[tuple[int, float]]

    _collection_name: str = "DailySessions"
