from pydantic import BaseModel


class User(BaseModel):
    name: str
    rating_position: int
    wallet_addr: str
    email: str
    today_choice: str
    points: float

    _collection_name: str = "Users"

