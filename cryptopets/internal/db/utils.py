import logging

from settings.db import MONGO_CONFIG
from .models import sessions, users, pets


async def create_collections(client):
    try:
        for model in [sessions.DailySession, pets.Pet, users.User]:
            await client[MONGO_CONFIG.MONGO_DB].create_collection(model._collection_name)
    except:
        logging.info("tables already exist")
